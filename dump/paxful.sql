-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2020 at 05:36 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paxful`
--

-- --------------------------------------------------------

--
-- Table structure for table `commission`
--

CREATE TABLE `commission` (
  `transfer_id` int(11) NOT NULL,
  `from_wallet` int(11) NOT NULL,
  `to_wallet` int(11) NOT NULL,
  `transfer_amount` float NOT NULL,
  `commission` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commission`
--

INSERT INTO `commission` (`transfer_id`, `from_wallet`, `to_wallet`, `transfer_amount`, `commission`) VALUES
(6, 3, 4, 300, 4.5),
(7, 2, 6, 900, 13.5),
(8, 2, 3, 100, 1.5),
(9, 2, 3, 100, 1.5),
(10, 2, 3, 100, 1.5),
(11, 2, 3, 50, 0.75),
(12, 2, 3, 50, 0.75),
(13, 4, 2, 50, 0.75),
(14, 2, 3, 50, 0.75),
(15, 1, 4, 50, 0.75),
(16, 2, 4, 50, 0.75),
(17, 2, 4, 50, 0.75),
(18, 2, 4, 50, 0.75),
(19, 2, 4, 50, 0.75),
(20, 4, 2, 50, 0.75),
(21, 4, 2, 50, 0.75),
(22, 2, 3, 100, 1.5),
(23, 2, 3, 100, 1.5),
(24, 3, 4, 200, 3),
(25, 3, 4, 200, 3),
(26, 2, 3, 1000, 15),
(27, 2, 3, 100, 1.5),
(28, 2, 3, 100, 1.5),
(29, 2, 3, 100, 1.5),
(30, 2, 3, 5, 0.075);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `wallet_id` int(11) NOT NULL,
  `balance` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`wallet_id`, `balance`) VALUES
(2, 544.25),
(3, 1382.43),
(4, 694),
(5, 300),
(6, 200),
(7, 600),
(8, 200),
(9, 1800),
(10, 900),
(11, 600);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `commission`
--
ALTER TABLE `commission`
  ADD PRIMARY KEY (`transfer_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`wallet_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `commission`
--
ALTER TABLE `commission`
  MODIFY `transfer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `wallet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
